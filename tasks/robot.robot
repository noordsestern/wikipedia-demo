# -*- coding: utf-8 -*-
*** Settings ***
Library    RPA.Browser
Task Teardown    Close All Browsers

# +
*** Keywords ***
Hauptseite von Wikipedia.de öffnen
    Open Available Browser    https://de.wikipedia.org   

Screenshot der Nachrichten erstellen
    Wait Until Page Contains Element    xpath://div[@id="hauptseite-nachrichten"]
    Capture Element Screenshot    xpath://div[@id="hauptseite-nachrichten"]
# -
*** Tasks ***
Screenshot der aktuellen Nachrichten auf Wikipedia erstellen
    Hauptseite von Wikipedia.de öffnen
    Screenshot der Nachrichten erstellen



